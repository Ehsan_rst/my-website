import React, { useEffect, useState } from 'react';
import { Background, Header, MyPortrait, MySkills } from '../components';
import Footer from '../components/Footer';
import { ChevronUp } from '../components/kits';

export default function Home() {
  const [scrollPosition, setScrollPosition] = useState(0);
  const handleScroll = () => {
    const position = window.pageYOffset;
    setScrollPosition(position);
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll, { passive: true });
    return () => window.removeEventListener('scroll', handleScroll);
  });

  return (
    <div className="position-relative">
      <div id="header">
        <Header />
      </div>
      {scrollPosition >= 700 && <ChevronUp />}
      <MyPortrait />
      <MySkills />
      <Background />
      <Footer />
    </div>
  );
}
