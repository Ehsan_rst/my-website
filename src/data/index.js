import myEducation from './myEducation';
import mySkills from './mySkills';
import myExperience from './myExperience';

export { myEducation, mySkills, myExperience };
