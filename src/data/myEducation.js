export default [
  {
    title: 'Master of Engineering Management',
    subTitle: 'Industrial Engineering, Industrial Management Institute',
    date: '2016 - 2019',
  },
  {
    title: 'Bachelor of Transportation Engineering',
    subTitle:
      'Industrial Engineering, Iran University of Science and Technology',
    date: '2010 - 2016',
  },
  {
    title: 'Pre-University Course Completion Certificate, Mathematics',
    subTitle: 'Mofid High School',
    date: '2006 - 2010',
  },
];
