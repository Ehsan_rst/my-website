import typography from './typography';
import colors from './colors';

export { typography, colors };
