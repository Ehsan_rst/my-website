export default {
  egyptianBlue: '#1148B2',
  solitude: '#F9FAFC',
  lavender: '#F1F4FB',
  white: '#FFFFFF',
  black: '#000000',
};
