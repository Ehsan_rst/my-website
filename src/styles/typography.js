export default {
  b36: {
    fontWeight: 'bold',
    fontSize: '36px',
  },
  b24: {
    fontWeight: 'bold',
    fontSize: '24px',
  },
  sb24: {
    fontWeight: '600',
    fontSize: '24px',
  },
  b18: {
    fontWeight: 'bold',
    fontSize: '18px',
  },
  rg18: {
    fontWeight: 'normal',
    fontSize: '18px',
  },
  sb15: {
    fontWeight: '600',
    fontSize: '15px',
  },
  lg14: {
    fontWeight: '300',
    fontSize: '14px',
  },
};
