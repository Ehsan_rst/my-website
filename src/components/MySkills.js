import React from 'react';
import styled from 'styled-components';
import { Container } from 'react-bootstrap';
import { mySkills } from '../data';
import { HR, Pill, Text } from './kits';

const Wrapper = styled.div`
  @media (min-width: 1200px) {
    padding-top: 150px;
    padding-bottom: 150px;
  }
`;

const StyledContainer = styled(Container)`
  @media (min-width: 1200px) {
    margin-top: 75px;
  }
  @media (min-width: 992px) {
    max-height: 400px;
  }
  @media (min-width: 768px) and (max-width: 991.9px) {
    max-height: 500px;
  }
`;

export default function MySkills() {
  return (
    <Wrapper id="skills">
      <Text className="d-none d-xl-block text-center" size="b36" color="black">
        My Skills
      </Text>
      <Text className="d-xl-none text-center mt-5" size="b24" color="black">
        My Skills
      </Text>
      <HR
        width={200}
        height={5}
        color="egyptianBlue"
        className="d-none d-xl-block mx-auto"
      />
      <HR
        width={125}
        height={2}
        color="egyptianBlue"
        className="d-xl-none mb-4 mx-auto"
      />
      <StyledContainer className="d-flex flex-xl-column flex-wrap justify-content-center align-items-center align-items-stretch align-content-around">
        {mySkills.map((skill, index) => (
          <React.Fragment key={index}>
            <Text className="d-none d-xl-block py-2" size="sb24" color="black">
              {skill}
            </Text>
            <Pill className="d-xl-none m-1">{skill}</Pill>
          </React.Fragment>
        ))}
      </StyledContainer>
    </Wrapper>
  );
}
