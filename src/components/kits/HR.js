import styled from 'styled-components';
import { colors } from '../../styles';

const HR = styled.div`
  width: ${(props) => props.width}px;
  border-top: ${(props) => props.height}px solid
    ${(props) => colors[props.color]};
  border-radius: 15px;
  border-color: ${(props) => props.color};
`;

export default HR;
