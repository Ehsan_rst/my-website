import React from 'react';
import { colors, typography } from '../../styles';

export default function Pill(props) {
  const { children, ...restProps } = props;
  return React.createElement('span', {
    ...restProps,
    style: {
      backgroundColor: colors.lavender,
      borderRadius: '15px',
      fontsize: typography.lg14.fontSize,
      fontWeight: typography.lg14.fontWeight,
      padding: '2.5px 10px',
    },
    children,
  });
}
