// @flow

import React from 'react';
import styled from 'styled-components';
import { colors } from '../../styles';
import Text from './Text';

type Props = {
  type: 'primary' | 'outline',
  size?: 'large' | 'medium',
};

const Wrapper = styled.div`
  width: 180px;
  height: ${(props) => (props.size === 'medium' ? '45px' : '55px')};
  border-radius: 30px;
  background-color: ${(props) =>
    props.type === 'primary' ? colors.egyptianBlue : colors.white};
  border: ${(props) =>
    props.type === 'outline' ? `3px solid ${colors.egyptianBlue}` : 'unset'};
`;

export default function Button(props: Props) {
  const { children, type, size = 'large' } = props;
  return (
    <Wrapper
      type={type}
      className="d-flex justify-content-center align-items-center"
      size={size}
    >
      <Text size="b18" color={type === 'primary' ? 'white' : 'egyptianBlue'}>
        {children}
      </Text>
    </Wrapper>
  );
}
