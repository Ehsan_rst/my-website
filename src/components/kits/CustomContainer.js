import styled from 'styled-components';

const CustomContainer = styled.div`
  min-width: ${(props) => props.minWidth};
  max-width: ${(props) => props.maxWidth};
`;

export default CustomContainer;
