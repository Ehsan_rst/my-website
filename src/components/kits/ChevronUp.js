import React from 'react';
import styled from 'styled-components';
import { colors } from '../../styles';

const Wrapper = styled.div`
  @media (min-width: 375px) {
    width: 50px;
    height: 50px;
    bottom: 20px;
    right: 20px;
  }

  @media (max-width: 374.5px) {
    width: 35px;
    height: 35px;
    bottom: 10px;
    right: 10px;
  }

  border-radius: 50%;
  background-color: ${colors.solitude};
  position: fixed;
  cursor: pointer;
`;

const handleScroll = (id) => {
  console.log(`id: ${id}`);
  const element = document.querySelector(id);
  element.scrollIntoView({ behavior: 'smooth' });
};

export default function ChevronUp() {
  return (
    <Wrapper
      onClick={() => handleScroll('#header')}
      className={`d-flex align-items-center justify-content-center`}
    >
      <svg
        className="bi bi-chevron-up"
        width="1em"
        height="1em"
        viewBox="0 0 16 16"
        fill={colors.egyptianBlue}
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fillRule="evenodd"
          d="M7.646 4.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1-.708.708L8 5.707l-5.646 5.647a.5.5 0 0 1-.708-.708l6-6z"
        />
      </svg>
    </Wrapper>
  );
}
