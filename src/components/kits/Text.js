// @flow

import React from 'react';
import { colors, typography } from '../../styles';

// TODO: How to map in colors keys instead of write each of them in front of color type props 👇?
type Props = {
  type?: string,
  size: 'b36' | 'b24' | 'sb24' | 'b18' | 'rg18' | 'sb15' | 'lg14',
  color: 'egyptianBlue' | 'solitude' | 'lavender' | 'white' | 'black',
};

export default function Text(props: Props) {
  const { type = 'div', children, size, color, ...restProps } = props;
  return React.createElement(
    type,
    {
      ...restProps,
      style: {
        color: colors[color],
        fontSize: typography[size].fontSize,
        fontWeight: typography[size].fontWeight,
      },
    },
    children
  );
}
