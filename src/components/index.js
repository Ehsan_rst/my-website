import Header from './Header';
import MyPortrait from './MyPortrait';
import SocialIcons from './SocialIcons';
import MySkills from './MySkills';
import Background from './Background';

export { Header, MyPortrait, SocialIcons, MySkills, Background };
