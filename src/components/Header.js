import React from 'react';
import styled from 'styled-components';
import { Text } from './kits';
import logo from '../assets/images/logo-black.svg';
import { Nav, Navbar } from 'react-bootstrap';

const Wrapper = styled.div`
  max-width: 850px;
`;

const Image = styled.img`
  margin-left: -10px;
`;

export default function Header() {
  const handleScroll = (id) => {
    const element = document.querySelector(id);
    element.scrollIntoView({ behavior: 'smooth' });
  };

  return (
    <>
      {/* Below Wrapper will be shown for lg screen */}
      <Wrapper className="d-none d-lg-flex align-items-md-center justify-content-md-between py-md-5 mx-md-auto">
        <img src={logo} alt="Logo" width={100} />
        <div className="d-flex px-5">
          <Text
            size="b24"
            color="black"
            className="pointer"
            onClick={() => handleScroll('#skills')}
          >
            Skills
          </Text>
          <span className="px-5">
            <Text
              className="pointer"
              onClick={() => handleScroll('#education')}
              size="b24"
              color="black"
            >
              Education
            </Text>
          </span>
          <Text
            className="pointer"
            onClick={() => handleScroll('#experience')}
            size="b24"
            color="black"
          >
            Experience
          </Text>
          <span className="px-5">
            <Text
              className="d-flex align-items-center position-relative"
              size="b24"
              color="black"
            >
              <span>Blog</span>
              <div
                className="position-absolute"
                style={{
                  fontSize: '12px',
                  width: '100px',
                  left: '55px',
                  top: 0,
                }}
              >
                (Coming Soon!)
              </div>
            </Text>
          </span>
        </div>
      </Wrapper>

      {/* Below Navbar will be shown for screens are smaller than lg */}
      <Navbar
        className="d-lg-none"
        collapseOnSelect
        expand="lg"
        bg="white"
        variant="light"
      >
        <Navbar.Brand>
          <Image src={logo} alt="Logo" width={70} />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link onClick={() => handleScroll('#skills')}>Skills</Nav.Link>
            <Nav.Link onClick={() => handleScroll('#m_education')}>
              Education
            </Nav.Link>
            <Nav.Link onClick={() => handleScroll('#m_experience')}>
              Experience
            </Nav.Link>
            <Nav.Link>
              <span>Blog</span>
              <span
                style={{
                  fontSize: '12px',
                  paddingLeft: '3px',
                }}
              >
                (Coming Soon!)
              </span>
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
}
