import React from 'react';
import styled from 'styled-components';
import { Container } from 'react-bootstrap';
import { CustomContainer, Text } from './kits';
import { colors } from '../styles';
import { SocialIcons } from './index';
import myPortrait from '../assets/images/me.png';
import myLittlePortrait from '../assets/images/my-little-portrait.jpg';

const Wrapper = styled.div`
  background-color: ${colors.solitude};
`;

const LimitedWrapper = styled.div`
  max-width: 2000px;
`;

const StyledImage = styled.img`
  transform: rotateY(180deg);
`;

const Avatar = styled.img`
  border-radius: 50%;
  border: 1px solid ${colors.lavender};
`;

export default function MyPortrait() {
  return (
    <>
      {/* Below Wrapper will be shown in xl screens */}
      <Wrapper className="d-flex justify-content-center">
        <LimitedWrapper className="d-none d-xl-flex align-items-xl-center px-lg-5">
          <CustomContainer minWidth="20%" className="d-flex">
            <SocialIcons alignment="vertical" />
          </CustomContainer>
          <CustomContainer minWidth="40%" className="d-flex flex-column">
            <Text size="b24" color="black">
              Hi, I am
            </Text>
            <Text size="b36" color="black">
              Mohammad Reza Ghasemi
            </Text>
            <Text size="b24" color="black">
              Front-End Web Developer
            </Text>
            <Text className="pt-5" size="rg18" color="black">
              I was interested in the computer, since childhood. My major was
              Industrial Engineering. I worked in this field for four years.
              Because of my interest in development, I decided to leave my
              carrier and be a professional web developer. I'm also interested
              in AI and Blockchain.
            </Text>
          </CustomContainer>
          <CustomContainer
            minWidth="40%"
            className="d-flex justify-content-center"
          >
            <StyledImage
              width={427}
              src={myPortrait}
              alt="Portrait of Mohammad Reza Ghasemi"
            />
          </CustomContainer>
        </LimitedWrapper>
      </Wrapper>

      {/* Below Wrapper will be shown in lg to xl screens */}
      <Wrapper className="d-none d-lg-flex d-xl-none align-items-lg-center ">
        <CustomContainer minWidth="10%">
          <SocialIcons alignment="vertical" />
        </CustomContainer>
        <CustomContainer minWidth="55%" className="d-flex flex-column pl-5">
          <Text size="b18" color="black">
            Hi, I am
          </Text>
          <Text size="b24" color="black">
            Mohammad Reza Ghasemi
          </Text>
          <Text size="b18" color="black">
            Front-End Web Developer
          </Text>
          <Text className="py-5 pr-5" size="rg18" color="black">
            I was interested in the computer, since childhood. My major was
            Industrial Engineering. I worked in this field for four years.
            Because of my interest in development, I decided to leave my carrier
            and be a professional web developer. I'm also interested in AI and
            Blockchain.
          </Text>
        </CustomContainer>
        <CustomContainer minWidth="35%" className="d-flex justify-content-end">
          <StyledImage
            width={360}
            src={myPortrait}
            alt="Portrait of Mohammad Reza Ghasemi"
          />
        </CustomContainer>
      </Wrapper>

      {/* Below Container will be shown in screens are smaller than lg */}
      <Wrapper>
        <Container className="d-flex flex-column align-items-center d-lg-none py-5">
          <Avatar
            src={myLittlePortrait}
            alt="Portrait of Mohammad Reza Ghasemi"
            width={115}
          />
          <Text className="text-center pt-4" size="sb15" color="black">
            Hi, I am
          </Text>
          <Text className="text-center" size="b18" color="black">
            Mohammad Reza Ghasemi
          </Text>
          <Text className="text-center" size="sb15" color="black">
            Front-End Web Developer
          </Text>
          <Text className="text-center pt-4" size="sb15" color="black">
            I was interested in the computer, since childhood. My major was
            Industrial Engineering. I worked in this field for four years.
            Because of my interest in development, I decided to leave my carrier
            and be a professional web developer. I'm also interested in AI and
            Blockchain.
          </Text>
        </Container>
      </Wrapper>
    </>
  );
}
